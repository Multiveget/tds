// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "UObject/NoExportTypes.h"
#include "Particles/ParticleSystemComponent.h"
#include "TPS_StateEffect.generated.h"


/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TPS_API UTPS_StateEffect : public UObject
{
	GENERATED_BODY()
	
public:
	virtual bool InitObject(AActor* Actor);
	//virtual void InitObject(APawn* Pawn);
	virtual void DestroyObject();



	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		bool bIsStackable = false;

	AActor* myActor = nullptr;
};

UCLASS()
class TPS_API UTPS_StateEffect_Execute_Once : public UTPS_StateEffect
{
	GENERATED_BODY()
public:
	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20;
};


UCLASS()
class TPS_API UTPS_StateEffect_Execute_Timer : public UTPS_StateEffect
{
	GENERATED_BODY()
public:
	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float Power = 20;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float Timer = 5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float Ratetime = 20;

	FTimerHandle ExecuteTimer;
	FTimerHandle EffectTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;

	void SpawnEmmiter(AActor* Interface, UParticleSystemComponent* &Emitter, UParticleSystem* Effect);
};