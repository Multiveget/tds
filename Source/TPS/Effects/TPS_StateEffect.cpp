// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_StateEffect.h"
#include "Kismet/GameplayStatics.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/Character/TPSHealthComponent.h"

void UTPS_StateEffect::DestroyObject()
{
	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}


bool UTPS_StateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;

	ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}

void UTPS_StateEffect_Execute_Once::DestroyObject()
{
	Super::DestroyObject();
}

void UTPS_StateEffect_Execute_Once::ExecuteOnce()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}

	DestroyObject();
}


bool UTPS_StateEffect_Execute_Once::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UTPS_StateEffect_Execute_Timer::DestroyObject()
{
	if(ParticleEmitter)
		ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UTPS_StateEffect_Execute_Timer::Execute()
{
	if (myActor)
	{
		//UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);	
		UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}

void UTPS_StateEffect_Execute_Timer::SpawnEmmiter(AActor* Interface, UParticleSystemComponent* &Emitter, UParticleSystem* Effect)
{
	auto myInterface = Cast<ITPS_IGameActor>(Interface);
	if (myInterface)
	{
		UActorComponent* myComponent = nullptr;
		if (myInterface->TryGetComponent(myComponent))
		{
			//���� ������ ���
			if (myComponent->GetClass() == UStaticMeshComponent::StaticClass())
			{
				Emitter = UGameplayStatics::SpawnEmitterAttached(Effect, Interface->GetRootComponent(), FName(), FVector::ZeroVector, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
			else
			{
				if (myComponent->GetClass() == USkeletalMeshComponent::StaticClass())
				{
					auto mySkelet = Cast<USkeletalMeshComponent>(myComponent);
					if (mySkelet)
					{
						if (mySkelet->GetCollisionEnabled() != ECollisionEnabled::PhysicsOnly)
						{
							FName BoneName = mySkelet->FindClosestBone(myInterface->BoneHit);
							Emitter = UGameplayStatics::SpawnEmitterAttached(Effect, Interface->GetRootComponent(), BoneName, FVector(0), FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
						}
					}

				}
			}
		}
	}
}

bool UTPS_StateEffect_Execute_Timer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(EffectTimer, this, &UTPS_StateEffect_Execute_Timer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(ExecuteTimer, this, &UTPS_StateEffect_Execute_Timer::Execute, Ratetime, true);

	if (ParticleEffect)
	{
		SpawnEmmiter(Actor, ParticleEmitter, ParticleEffect);
		//ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), FName(), FVector::ZeroVector, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	}

	return true;
}
