// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Interface/TPS_EnvironmentStructure.h"
#include "TPS_EnvironmentStructure.h"
#include "PhysicalMaterials/Physicalmaterial.h"

// Sets default values
ATPS_EnvironmentStructure::ATPS_EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATPS_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPS_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATPS_EnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh=Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		auto myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return result;
}

void ATPS_EnvironmentStructure::RemoveEffect(UTPS_StateEffect* RemovedEffect)
{
	Effects.Remove(RemovedEffect);
}

void ATPS_EnvironmentStructure::AddEffect(UTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}

bool ATPS_EnvironmentStructure::TryGetComponent(UActorComponent* &myComponent)
{
	auto mySkelet = Cast<USkeletalMeshComponent>(GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (mySkelet)
	{
		myComponent = mySkelet;
		return true;
	}
	else
	{
		auto myStaticMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
		if (myStaticMesh)
		{
			myComponent = myStaticMesh;
			return true;
		}
		else
			return false;
	}
}

TArray<UTPS_StateEffect*> ATPS_EnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}



