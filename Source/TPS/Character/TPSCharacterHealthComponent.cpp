// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Character/TPSCharacterHealthComponent.h"
#include "TPS/Character/TPSCharacterHealthComponent.h"

void UTPSCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTPSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	if (Shield > 100)
		Shield = 100;
	else
	{
		if (Shield < 0)
			Shield = 0;
	}

	GetWorld()->GetTimerManager().SetTimer(ShieldCoolDownTimer, this, &UTPSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime, false);
	GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UTPSCharacterHealthComponent::CoolDownShieldEnd()
{
	GetWorld()->GetTimerManager().SetTimer(ShieldRecoveryRateTimer, this, &UTPSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
}

void UTPSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp += ShieldRecoveryValue;
	if (tmp > 100)
	{
		Shield = 100;
		GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
	}
	else
		Shield = tmp;
	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}
