// Fill out your copyright notice in the Description page of Project Settings.
#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "TPS/Character/TPSInventoryComponent.h"
#include "TPS/Character/TPSCharacter.h"
#include "TPS/Effects/TPS_StateEffect.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	/*if (GetWeaponRound() > 0)
	{
		if (WeaponFiring)
			if (FireTimer < 0.f)
			{
				if (!WeaponReloading)
					Fire();
			}
			else
				FireTimer -= DeltaTime;
	}
	else
	{
		if (!WeaponReloading && CheckCanWeaponReload())
		{
			InitReload();
		}
	}*/
	if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
	{
		if (FireTimer < 0)
			Fire();
		else
			FireTimer -= DeltaTime;
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{

			CurrentDispersion = CurrentDispersionMin;

		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	/*if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);*/
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}

	UpdateStateWeapon(EMovementState::Run_State);
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
	FireTimer = 0.01f;//!!!!!
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		auto myInventory=Cast<UTPSInventoryComponent>(GetOwner()->GetComponentByClass(UTPSInventoryComponent::StaticClass()));
		if (myInventory)
		{
			int8 AvalableAmmoForWeapon;
			if (!myInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType,AvalableAmmoForWeapon))
			{
				result = false;
			}
		}
	}
	return result;
}

int8 AWeaponDefault::GetAvalableAmmoForReload()
{
	int8 AvalableAmmoForWeapon = WeaponSetting.MaxRound;
	//int8 result = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		auto myInventory = Cast<UTPSInventoryComponent>(GetOwner()->GetComponentByClass(UTPSInventoryComponent::StaticClass()));
		if (myInventory)
		{
			if (!myInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType,AvalableAmmoForWeapon))
			{
				AvalableAmmoForWeapon = AvalableAmmoForWeapon;
			}
		}
	}
	return AvalableAmmoForWeapon;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSetting.RateOfFire;
	AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round - 1;
	ChangeDispersionByShot();
	auto myPlayer = Cast<ATPSCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(),0));
	if (myPlayer)
	{
		if (myPlayer->AimEnabled)
			myPlayer->PlayAnimMontage(WeaponSetting.AnimCharFireIronSight);
		else
			myPlayer->PlayAnimMontage(WeaponSetting.AnimCharFire);
	}
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());

	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)//Shotgun
		{
			EndLocation = GetFireEndLocation();

			FVector Dir = EndLocation - SpawnLocation;

			Dir.Normalize();

			FMatrix myMatrix(Dir, FVector(0, 0, 0), FVector(0, 0, 0), FVector::ZeroVector);
			SpawnRotation = myMatrix.Rotator();
			AProjectileDefault* myProjectile;
			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
			}
			else
			{
				EDrawDebugTrace::Type myTrace;
				if (ShowDebug)
					myTrace = EDrawDebugTrace::Persistent;
				else
					myTrace = EDrawDebugTrace::None;
				FHitResult TraceHitResult;
				FVector TraceEndLocation = SpawnLocation + WeaponSetting.DistacneTrace * Dir;
				FCollisionQueryParams TraceParams(TEXT("Trace"), false, this);
				TraceParams.bReturnPhysicalMaterial = true;
				GetWorld()->LineTraceSingleByChannel(TraceHitResult,SpawnLocation,TraceEndLocation, ECollisionChannel::ECC_Visibility,TraceParams);
				if (TraceHitResult.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(TraceHitResult);
					if (WeaponSetting.TraceHitDecals.Contains(mySurfacetype))
					{
						UMaterialInterface* myMaterial = WeaponSetting.TraceHitDecals[mySurfacetype];
						if (myMaterial)
							UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20), TraceHitResult.GetComponent(), NAME_None, TraceHitResult.ImpactPoint, TraceHitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
					}
					if (WeaponSetting.TraceHitFXs.Contains(mySurfacetype))
					{
						UParticleSystem* myParticle = WeaponSetting.TraceHitFXs[mySurfacetype];
						if (myParticle)
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, TraceHitResult.ImpactPoint, TraceHitResult.ImpactNormal.Rotation(), FVector(1));
					}
					if (WeaponSetting.TraceHitSound)
						UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.TraceHitSound, TraceHitResult.ImpactPoint);

					UGameplayStatics::ApplyPointDamage(TraceHitResult.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, TraceHitResult.TraceStart, TraceHitResult, GetInstigatorController(), this, NULL);

					

					UTypes::AddEffectBySurfaceType(TraceHitResult.GetActor(), ProjectileInfo.Effect, mySurfacetype);

					/*if (TraceHitResult.GetActor()->GetClass()->ImplementsInterface(UTPS_IGameActor::StaticClass()))
					{
						ITPS_IGameActor::Execute_AvalableForEffects(TraceHitResult.GetActor());
						ITPS_IGameActor::Execute_AvalableForEffectsBP(TraceHitResult.GetActor());
					}*/
					//UTPS_StateEffect* NewEffect=NewObject<UTPS_StateEffect>(TraceHitResult.GetActor(), FName("Effect"))
				}

				//ToDo Projectile null Init trace fire			

				//GetWorld()->LineTraceSingleByChannel()
			}
			auto myPlayerInventoryComponent = Cast<UTPSInventoryComponent>(myPlayer->InventoryComponent);
			if (myPlayerInventoryComponent)
				myPlayerInventoryComponent->SetAdditionalInfoWeapon(myPlayer->CurrentIndexWeapon, AdditionalWeaponInfo);
			AStaticMeshActor* Shell = nullptr;
			if (WeaponSetting.ShellBullets)
			{
				FVector ShellSpawnLocation = SkeletalMeshWeapon->GetSocketLocation(FName("shell_socket"));
				FRotator ShellSpawnRotation = FRotator(0);
				//FVector ShellSpawnScale = FVector(0.01, 0.01, 0.1);
				FVector ShellSpawnScale = FVector(1);
				FTransform ShellSpawnTransform = FTransform(ShellSpawnRotation, ShellSpawnLocation, ShellSpawnScale);
				FActorSpawnParameters ShellSpawnParameters;
				ShellSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				Shell = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), ShellSpawnTransform, ShellSpawnParameters);
				auto ShellMeshComponent = Shell->GetStaticMeshComponent();
				EComponentMobility::Type ShellMeshComponentMobility = EComponentMobility::Movable;
				ShellMeshComponent->SetMobility(ShellMeshComponentMobility);
				ShellMeshComponent->SetStaticMesh(WeaponSetting.ShellBullets);
				ShellMeshComponent->SetSimulatePhysics(true);
				FVector WeaponDirection = GetActorForwardVector();
				FVector ShellDirection = FVector(WeaponDirection.X, WeaponDirection.Y, 0) * (-1) * WeaponSetting.ProjectileSetting.ProjectileInitSpeed;
				float CosX = ShellDirection.CosineAngle2D(FVector(1, 0, 0));
				float CosY = ShellDirection.CosineAngle2D(FVector(0, 1, 0));
				FVector ShellImpulseDirection = FVector(CosX, CosY, 0);
				ShellMeshComponent->AddImpulse(ShellImpulseDirection);
			}
		}
	}
	if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		if (CheckCanWeaponReload())
			InitReload();
	}
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	OnWeaponReloadEnd.Broadcast(false,0);

}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	//ToDo Dispersion
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Run_State:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::SprintRun_State:
		BlockFire = true;
		SetWeaponStateFire(false);//set fire trigger to false
		//Block Fire
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	//UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistacneTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistacneTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}


	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}


	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;
	AStaticMeshActor* Mag=nullptr;
	//auto MagMeshComponent = Mag->GetStaticMeshComponent();
	ReloadTimer = WeaponSetting.ReloadTime;
	if (WeaponSetting.MagazineDrop)
	{
		FVector MagSpawnLocation=SkeletalMeshWeapon->GetSocketLocation(FName("gun_mag_spawn_socket"));
		FRotator MagSpawnRotation = FRotator(0);
		FVector MagSpawnScale = FVector(1);
		FTransform MagSpawnTransform = FTransform(MagSpawnRotation, MagSpawnLocation, MagSpawnScale);
		FActorSpawnParameters MagSpawnParameters;
		MagSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		Mag = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(),MagSpawnTransform,MagSpawnParameters);
		auto MagMeshComponent = Mag->GetStaticMeshComponent();
		EComponentMobility::Type MagMeshComponentMobility = EComponentMobility::Movable;
		MagMeshComponent->SetMobility(MagMeshComponentMobility);
		MagMeshComponent->SetStaticMesh(WeaponSetting.MagazineDrop);
		MagMeshComponent->SetSimulatePhysics(true);
	}
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundReloadWeapon, ShootLocation->GetComponentLocation());
	//ToDo Anim reload
	if (WeaponSetting.AnimCharReload)
		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload);
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	int8 AvalableAmmoFromInventory = GetAvalableAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;
	if (NeedToReload > AvalableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Round = AvalableAmmoFromInventory;
		AmmoNeedTakeFromInv = AvalableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}


	/*if (AvalableAmmoFromInventory > WeaponSetting.MaxRound)
		AvalableAmmoFromInventory = WeaponSetting.MaxRound;

	int32 AmmoNeedTake = AdditionalWeaponInfo.Round;
	AmmoNeedTake -= AvalableAmmoFromInventory;
	AdditionalWeaponInfo.Round = AvalableAmmoFromInventory;*/

	OnWeaponReloadEnd.Broadcast(true,-AmmoNeedTakeFromInv);
}

